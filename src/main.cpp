#include <iostream>
#include "Line.h"
#include "pingReturns.h"
#include "getLatency.h"
#include "playSound.h"

int main()
{
    Line pingOutput;
    while (pingOutput.loadLine()) {
        std::cout << pingOutput.get() << std::endl;
        if (pingReturns(pingOutput)) {
            const float latency = getLatency(pingOutput);
            if (latency!=-1) playSound(latency);
        }
    }

    return 0;
}
