#ifndef PING_EARS_LINE_H
#define PING_EARS_LINE_H

#include <iostream>
#include <vector>
#include "getNextWord.h"

class Line {
private:
    std::string line;
    std::vector<std::string> words;

    void splitLineToWords()
    {
        words.clear();
        for (unsigned long i = 0; i<line.length(); i++) {
            words.push_back(getNextWord(line, i));
        }
    }

public:

    Line() { }

    Line(std::string str)
            :line(std::move(str))
    {
        splitLineToWords();
    }

    Line& operator=(std::string newLineContents)
    {
        line = std::move(newLineContents);
        splitLineToWords();
        return *this;
    }

    std::string operator[](const unsigned long index) const
    {
        return words[index];
    }

    std::string get() const { return line; }

    unsigned long wordsAmount() const { return words.size(); };

    std::istream& loadLine(std::istream& is = std::cin)
    {
        std::getline(is, line);
        splitLineToWords();
        return is;
    }
};

#endif //PING_EARS_LINE_H
