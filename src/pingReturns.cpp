#include "Line.h"
#include "isANumber.h"

bool pingReturns(const Line& pingOutput)
{
    return isAnInteger(pingOutput[0]);
}
